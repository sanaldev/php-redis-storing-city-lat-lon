<?php

$redis = new Redis(); 
$redis->connect('localhost', 6379);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP);
$redis->setOption(Redis::OPT_PREFIX, 'CITY:');
//$info = $redis->info();

//var_dump($info);

class City //implements JsonSerializable
{
    public $cityid = 0;
    public $city = "";
    public $latitude = 0.00000;
	public $longitude = 0.00000;
	public $northeast_lat = 0.00000;
	public $northeast_lon = 0.00000;
    public $southwest_lat = 0.00000;
	public $southwest_lon = 0.00000;
	public $population = 0;
   
    public function getCityId() {
        return $this->cityid;
    }
   
    public function setCityId($cityid) {
        $this->cityid = $cityid;
    }
   
    public function getCity() {
        return $this->city;
    }

	public function setCity($city) {
        $this->city = $city;
    }

	public function getLatitude() {
        return $this->latitude;
    }

	public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

	public function getLongitude() {
        return $this->longitude;
    }

	public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

	public function getNorthEast_Lat() {
        return $this->northeast_lat;
    }

	public function setNorthEast_Lat($northeast_lat) {
        $this->northeast_lat = $northeast_lat;
    }

	public function getNorthEast_Lon() {
        return $this->northeast_lon;
    }

	public function setNorthEast_Lon($northeast_lon) {
        $this->northeast_lon = $northeast_lon;
    }

	public function getSouthWest_Lat() {
        return $this->southwest_lat;
    }

	public function setSouthWest_Lat($southwest_lat) {
        $this->southwest_lat = $southwest_lat;
    }

	public function getSouthWest_Lon() {
        return $this->southwest_lon;
    }

	public function setSouthWest_Lon($southwest_lon) {
        $this->southwest_lon = $southwest_lon;
    }

    public function getPopulation() {
        return $this->population;
    }

	public function setPopulation($population) {
        $this->population = $population;
    }

	//function jsonSerialize() {
   //     $data = array();
        // Represent your object using a nested array or stdClass,
        // in the way you want it arranged in your API
   //     return $data;
   // }
   
}

$redis->delete('citylocation'); // to delete all data for this specific key

$city_object = new City();

$row = 1;
$id = 1 ;
if (($handle = fopen("Australia_Cities.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        //echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        for ($c=0; $c < $num; $c++) {
            //echo $data[$c] . "<br />\n";

			if($c==0)
				$city_object->setCityId($data[$c]);
			if($c==1)
				$city_object->setCity($data[$c]);
			if($c==2)
				$city_object->setLatitude($data[$c]);
			if($c==3)
				$city_object->setLongitude($data[$c]);
			if($c==4)
				$city_object->setNorthEast_Lat($data[$c]);
			if($c==5)
				$city_object->setNorthEast_Lon($data[$c]);
			if($c==6)
				$city_object->setSouthWest_Lat($data[$c]);
			if($c==7)
				$city_object->setSouthWest_Lon($data[$c]);
			if($c==8)
				$city_object->setPopulation($data[$c]);
        }
		
		
		
		//$serialized = serialize($json_obj);
        //$json_obj = json_encode( (array)$city_object );
		//$redis->hSet('citylocation',$id, $json_obj);
		$redis->hSet('citylocation',$id, $city_object);
		//echo $redis->hGet('citylocation',$id);
		
		
		$id++;

    }
    fclose($handle);
}

var_dump($redis->hGetAll('citylocation'));
//$data = $redis->hGetAll('citylocation');

//foreach ($data as $key => $value)
//{
//  echo 'key --' . $key;
//	echo "<br />\n";
	
//	echo 'value --' .$value;
//	echo "<br />\n";
//	echo "<br />\n";
//}

?>